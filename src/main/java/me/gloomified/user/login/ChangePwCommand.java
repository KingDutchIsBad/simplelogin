package me.gloomified.user.login;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChangePwCommand implements CommandExecutor {
    Login login = Login.getPlugin(Login.class);
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            if(args[0] != null && args.length == 2) {
                    if (sender.hasPermission("Login.changepwadmin")) {
                        Player player = Bukkit.getPlayer(args[0]);
                        if (Login.playersLoggedIn.contains(player) && Login.get().get("Passwords." + player.getUniqueId()) != null) {
                            if (login.getConfig().getString("Encryption").equalsIgnoreCase("Argon2")) {
                                Encrypt.getEncryptClass().encrypt(args[1], player.getUniqueId(), Encrypt.EncryptType.ARGON2);
                                player.sendMessage(Login.translateToColorCode(login.getConfig().getString("Messages.ChangedPwSuccessfully")));
                            } else {
                                Encrypt.getEncryptClass().encrypt(args[1], player.getUniqueId(), Encrypt.EncryptType.AES);
                                player.sendMessage(Login.translateToColorCode(login.getConfig().getString("Messages.ChangedPwSuccessfully")));
                            }

                        }
                    }else {
                        sender.sendMessage(Login.translateToColorCode("Messages.PlayerTriedToUseAdminChangePw"));
                    }
            } else if (args[0] != null){
                if(sender.hasPermission("Login.changepwnormal")){
                    if(args.length == 1){
                        Player player = (Player) sender;
                        if(Login.playersLoggedIn.contains(player) && Login.get().get("Passwords." + player.getUniqueId()) != null){
                            if(login.getConfig().getString("Encryption").equalsIgnoreCase("Argon2")){
                                Encrypt.getEncryptClass().encrypt(args[0], player.getUniqueId() , Encrypt.EncryptType.ARGON2);
                                player.sendMessage(Login.translateToColorCode(login.getConfig().getString("Messages.ChangedPwSuccessfully")));
                            }else {
                                Encrypt.getEncryptClass().encrypt(args[0], player.getUniqueId() , Encrypt.EncryptType.AES);
                                player.sendMessage(Login.translateToColorCode(login.getConfig().getString("Messages.ChangedPwSuccessfully")));
                            }

                        }
                    }else {
                        sender.sendMessage(Login.translateToColorCode(login.getConfig().getString("Messages.PlayerTriedToChangePwButItHasTooManyArguments")));
                    }

                }else {
                    sender.sendMessage(Login.translateToColorCode(login.getConfig().getString("Messages.PlayerTriedToChangePwHDoesn'tHavePerms")));
                  }
            }else {
                sender.sendMessage(Login.translateToColorCode(login.getConfig().getString("Messages.PlayerTriedToPwButItDoesn'tHaveArgument0")));
            }

        }
        return true;
    }
    }
