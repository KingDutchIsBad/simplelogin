package me.gloomified.user.login;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

public class Encrypt {
    public enum EncryptType {
        AES,
        ARGON2
    }
        protected static final Encrypt getEncryptClass = new Encrypt();
        private static final String key = "aesEncryptionKey";
        private static final String initVector = "encryptionIntVec";
         Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id);
        protected static Encrypt getEncryptClass(){
            return getEncryptClass;
        }

         public void encrypt(String s , UUID uuid, EncryptType encrypt){
            if(encrypt.equals(Encrypt.EncryptType.ARGON2)){
                // Read password from user
                char[] password = s.toCharArray();
                String hash = "";
                try {
                    // Hash password
                    hash = argon2.hash(10, 65536, 1, password);
                } finally {
                    // Wipe confidential data
                    argon2.wipeArray(password);
                    Login.get().set("Passwords." + uuid, hash);
                    Login.save();
                }
            }else{
                try {
                    IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
                    SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
                    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
                    cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
                    byte[] encrypted = cipher.doFinal(s.getBytes());
                    Login.get().set("Passwords." + uuid, Base64.getEncoder().encodeToString(encrypted));
                    Login.save();
                    Base64.getEncoder().encodeToString(encrypted);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
         }
        public String verifyAes(String encrypted){
                try{ IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
                    SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
                    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
                    cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
                    byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
                    return new String(original);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            return null;
        }
        public boolean verifyArgon(String encrypted , UUID uuid){
            return argon2.verify(Login.get().get("Passwords." + uuid.toString()).toString() , encrypted);
        }
}
