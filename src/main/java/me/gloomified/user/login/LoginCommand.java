package me.gloomified.user.login;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LoginCommand  implements CommandExecutor {
    Login plugin = Login.getPlugin(Login.class);

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (Login.get().get("Passwords." + player.getUniqueId()) != null) {
                    if (plugin.getConfig().getString("Encryption").equalsIgnoreCase("AES")) {
                        if (args[0].equalsIgnoreCase(Encrypt.getEncryptClass().verifyAes(Login.get().get("Passwords." + player.getUniqueId()).toString()))) {
                            player.sendMessage(Login.translateToColorCode(plugin.getConfig().get("Messages.SuccessfullLogin").toString()));
                            Login.playersLoggedIn.add(player);
                        } else {
                            player.sendMessage(Login.translateToColorCode(plugin.getConfig().get("Messages.UnSuccessfullLogin").toString()));
                            if (plugin.getConfig().getBoolean("KickOnWrongPassword")) {
                                player.kickPlayer(Login.translateToColorCode(plugin.getConfig().getString("Messages.MessageOnKickIfPlayerPutWrongPassword")));
                            }
                        }
                    } else {
                        if (Encrypt.getEncryptClass().verifyArgon(args[0] , player.getUniqueId())) {
                            player.sendMessage(Login.translateToColorCode(plugin.getConfig().get("Messages.SuccessfullLogin").toString()));
                            Login.playersLoggedIn.add(player);
                        } else {
                            player.sendMessage(Login.translateToColorCode(plugin.getConfig().get("Messages.UnSuccessfullLogin").toString()));
                            if (plugin.getConfig().getBoolean("KickOnWrongPassword")) {
                                player.kickPlayer(Login.translateToColorCode(plugin.getConfig().getString("Messages.MessageOnKickIfPlayerPutWrongPassword")));
                            }
                        }
                    }
                }
            }
        }
            return true;
    }
}
