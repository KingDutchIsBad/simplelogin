package me.gloomified.user.login;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class Events implements Listener {
    Login plugins = Login.getPlugin(Login.class);
    @EventHandler
    public void onWalkEvent(PlayerMoveEvent event){
        if(Login.get().get("Passwords." + event.getPlayer().getUniqueId()) == null){
            event.setCancelled(true);
            event.getPlayer().sendMessage(Login.translateToColorCode(plugins.getConfig().get("Messages.PlayerJustJoinedHasntRegistered").toString()));
        }else if(!Login.playersLoggedIn.contains(event.getPlayer())){
            event.getPlayer().sendMessage(Login.translateToColorCode(plugins.getConfig().get("Messages.PlayerJustJoinedHasntLoggedIn").toString()));
            event.setCancelled(true);
        }else if(Login.playersLoggedIn.contains(event.getPlayer())){
            event.setCancelled(false);
        }
    }
    @EventHandler
    public void onPlayerSendCommandEvent(PlayerCommandPreprocessEvent event){
        if(!event.getMessage().contains("/login")  && !event.getMessage().contains("/register")){
            if(Login.get().get("Passwords." + event.getPlayer().getUniqueId()) == null){
                event.setCancelled(true);
                event.getPlayer().sendMessage(Login.translateToColorCode(plugins.getConfig().get("Messages.PlayerTriedToUseACommandHasntRegistered").toString()));
            }else if(!Login.playersLoggedIn.contains(event.getPlayer())){
                event.getPlayer().sendMessage(Login.translateToColorCode(plugins.getConfig().get("Messages.PlayerTriedToUseACommandHasntLoggedIn").toString()));
                event.setCancelled(true);
            }else if(Login.playersLoggedIn.contains(event.getPlayer())){
                event.setCancelled(false);
            }
        }
    }
    @EventHandler
    public void onPlayerChatEvent(AsyncPlayerChatEvent event){
        if(Login.get().get("Passwords." + event.getPlayer().getUniqueId()) == null){
            event.setCancelled(true);
            event.getPlayer().sendMessage(Login.translateToColorCode(plugins.getConfig().get("Messages.PlayerTriedToUseACommandHasntRegistered").toString()));
        }else if(!Login.playersLoggedIn.contains(event.getPlayer())){
            event.getPlayer().sendMessage(Login.translateToColorCode(plugins.getConfig().get("Messages.PlayerTriedToUseACommandHasntLoggedIn").toString()));
            event.setCancelled(true);
        }
    }

}
