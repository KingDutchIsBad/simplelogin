package me.gloomified.user.login;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public final class Login extends JavaPlugin implements CommandExecutor {
    protected static ArrayList<Player> playersLoggedIn = new ArrayList<>();
    private static File file;
    private  static FileConfiguration customFile;
    

    @Override
    public void onEnable() {
        setup();
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        getCommand("register").setExecutor(this);
        getCommand("login").setExecutor(new LoginCommand());
        getServer().getPluginManager().registerEvents(new Events() , this);
        getCommand("LoginReload").setExecutor(new ReloadCommand());
        getCommand("changepw").setExecutor(new ChangePwCommand());
    }
    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
    protected static String translateToColorCode(String s){
        return ChatColor.translateAlternateColorCodes('&' , s);
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
       if(sender instanceof Player) {
           switch(getConfig().getString("Encryption")){
               case "AES":
                   if (args.length == 1) {
                       if (get().get("Passwords." + ((Player) sender).getUniqueId()) == null) {
                           Player player = (Player) sender;
                           Encrypt.getEncryptClass().encrypt(args[0], player.getUniqueId() , Encrypt.EncryptType.AES);
                           player.sendMessage(translateToColorCode(getConfig().get("Messages.Registered").toString()));
                       } else {
                           sender.sendMessage(translateToColorCode(getConfig().get("Messages.AlreadyRegistered").toString()));
                       }
                   }
                   break;
               case "Argon2":
               default:
                   if (args.length == 1) {
                       if (get().get("Passwords." + ((Player) sender).getUniqueId()) == null) {
                           Player player = (Player) sender;
                           Encrypt.getEncryptClass().encrypt(args[0], player.getUniqueId() , Encrypt.EncryptType.ARGON2);
                           player.sendMessage(translateToColorCode(getConfig().get("Messages.Registered").toString()));
                       } else {
                           sender.sendMessage(translateToColorCode(getConfig().get("Messages.AlreadyRegisterd").toString()));
                       }
                   }
                   break;
           }
       }
     return true;
    }
    public static  void setup(){
        file = new File(Bukkit.getServer().getPluginManager().getPlugin("SimpleLogin").getDataFolder(), "data.yml");

        if (!file.exists()){
            try{
                file.createNewFile();
            }catch (IOException e){
                //nothing really
            }
        }
        customFile = YamlConfiguration.loadConfiguration(file);
    }
    public  static FileConfiguration get(){
        return customFile;
    }

    public static void save(){
        try{
            customFile.save(file);
        }catch (IOException e){
            System.out.println("Couldn't save file");
        }
    }
    public static   void reload(){
         customFile = YamlConfiguration.loadConfiguration(file);
    }
}
